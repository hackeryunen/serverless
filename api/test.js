const request = require("request");

module.exports = (req, res) => {

    let host = "zfaka.lfcqjd.com";
    let rurl = "http://" + host + req.url;

    let ops = {
        method: req.method,
        encoding: null,
        headers: {}
    }

    //忽略的请求方式
    if (ops.method == "OPTIONS") {
        res.json({ code: 200 });
    } else {
        // 保留的头部键
        /*
        let keephd = ['user-agent', 'accept', 'accept-encoding', 'cache-control', 'content-type', 'cookies', 'referer', 'token', 'authorization'],
            headeryes = (req.headers["headeryes"] || "").split(','),
            //不保留的头部键
            headerno = (req.headers["headerno"] || "").split(',');

        keephd = keephd.concat(headeryes);

        for (var i in req.headers) {
            if (headerno.indexOf(i.toLowerCase()) == -1 && keephd.indexOf(i.toLowerCase()) >= 0) {
                ops.headers[i] = req.headers[i];
            }
        }
        */

        for (var i in req.headers) {
            ops.headers[i] = req.headers[i];
        }

        ops.headers['host'] = host;

        // 是否带 body
        if (["POST", "PUT", "PATCH", "DELETE"].indexOf(ops.method) >= 0) {
            let bk = [];
            for (var i in req.body) {
                bk.push(i + '=' +  encodeURIComponent(req.body[i]));
            }
            ops.body = bk.join('&');
        }

        request(rurl, ops, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                let rh = response.headers, ct = rh["content-type"] || "text/html; charset=utf-8";
                res.setHeader('content-type', ct);
                if(rh['content-encoding']) {
                    res.setHeader('content-encoding', rh['content-encoding']);
                }
                if (rh['set-cookie']) {
                    res.setHeader('set-cookie', rh['set-cookie']);
                }
                res.send(body);
            } else {
                res.json({
                    code: -1,
                    url: rurl,
                    msg: error + ""
                });
            }
        })
    }
}